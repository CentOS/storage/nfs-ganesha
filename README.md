This is a dist-git like repo for [NFS-Ganesha](https://github.com/nfs-ganesha/nfs-ganesha).

The master brach is unused. Use an existing branch instead.
Branch names follow convention like `c<VERSION>-sig-storage-nfs-ganesha-<GANESHA-VERSION>` as descibed on [Naming and Patterns for Mapping Git Branches to Koji Tags](https://wiki.centos.org/BrianStinson/GitBranchesandKojiTags)

* c7-sig-storage-nfs-ganesha-28: CentOS-7, nfs-ganesha-28
* c7-sig-storage-nfs-ganesha-30: CentOS-7, nfs-ganesha-30
* ...

Instructions for building the libntirpc and nfs-ganesha packages for the CentOS Storage SIG can be found in the following places:

* [Community Build System](https://wiki.centos.org/HowTos/CommunityBuildSystem)
* [Storage SIG landing page](https://wiki.centos.org/SpecialInterestGroup/Storage/Gluster)

To build from the source tar file in the look-aside cache:

`cbs build [--scratch] storage7-nfsganesha-2.8-el7 ssh://git.centos.org/rpms/libnfs-ganesha#$gitcommit`

Build the src.rpm with:


    $ rpmbuild -bs \
               --define "_sourcedir $PWD/SOURCES" --define "_srcrpmdir $PWD" \
               --define "dist .el7" SPECS/nfs-ganesha.spec

To build:

$ cbs build [--scratch] storage7-nfsganesha-28-el7 nfs-ganesha-2.8.3-1.el7.src.rpm

